package com.example.place_its;

import com.google.android.gms.maps.model.LatLng;

public class reminders {
	public String title;
	public String description;
	public LatLng position;
	public boolean repost;
		
	public reminders(String title, String description, LatLng position) {
		this.title = title;
		this.description = description;
		this.position = position;
	}
	
	public boolean getRepost(){
		return this.repost;
	}
	
	public void setRepost(boolean repost){
		this.repost = repost;
	}
	
	/*public String getRepostDate(){
		
	}*/
	
	/*public boolean isActive(){
		
	}*/
}
