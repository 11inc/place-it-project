package com.example.place_its;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class PlaceIts extends Activity implements OnMapClickListener, CancelableCallback {
    public final static String EXTRA_MESSAGE = "com.example.place_its.MESSAGE";

	private GoogleMap mMap;
	private List<Marker> mMarkers = new ArrayList<Marker>();
	private Iterator<Marker> marker;
	private List<reminders> ActiveList = new ArrayList<reminders>();
	private Iterator<reminders> remind;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_its);
		setUpMapIfNeeded();
		mMap.setMyLocationEnabled(true);
		mMap.setOnMapClickListener(this);
		
		Button btnReTrack = (Button) findViewById(R.id.retrack);
		btnReTrack.setOnClickListener(new View.OnClickListener() { 
			@Override
			public void onClick(View v) {
				marker = mMarkers.iterator();
				if (marker.hasNext()) {
					Marker current = marker.next();
					mMap.animateCamera(CameraUpdateFactory.newLatLng(current.getPosition()), 2000, PlaceIts.this);
					current.showInfoWindow();
				}
			}
		 });

	}
	
	private void setUpMapIfNeeded() {
	    // Do a null check to confirm that we have not already instantiated the map.
	    if (mMap == null) {
	        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	                            .getMap();
	        // Check if we were successful in obtaining the map.
	        if (mMap != null) {
	            // The Map is verified. It is now safe to manipulate the map.

	        }
	    }
	}
	
	@Override
	 public void onMapClick(LatLng position) {
		final LatLng pos = position;
		
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		
		LinearLayout lila1= new LinearLayout(this);
		lila1.setOrientation(1); //1 is for vertical orientation
		final EditText input = new EditText(this); 
		final EditText input1 = new EditText(this);
		lila1.addView(input);
		lila1.addView(input1);
		alert.setView(lila1);
		
		alert.setTitle("New Reminder");
		alert.setMessage("Please enter a Reminder Title and Description:");
		
		alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				String des = input1.getText().toString();
				Toast.makeText(PlaceIts.this, "Reminder added!", Toast.LENGTH_SHORT).show();
				Marker added = mMap.addMarker(new MarkerOptions()
				.position(pos)
				.title(value)
				.snippet(des));
					added.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.fb));
					mMarkers.add(added);
					
				ActiveList.add(new reminders(value,des,pos));
			}
		});
		
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Toast.makeText(PlaceIts.this, "Nothing added!", Toast.LENGTH_SHORT).show();
			}
		});
	 
		alert.show();
	 }
	
	/** Called when the user clicks the Send button */
	public void showActiveList(View view) {
	    // Do something in response to button
		Intent intent = new Intent(this, ActiveList.class);
		startActivity(intent);
	}

	@Override
	public void onCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	 public void onFinish() {
		if (marker.hasNext()) {
			Marker current = marker.next();
			mMap.animateCamera(CameraUpdateFactory.newLatLng(current.getPosition()), 2000, this);
			current.showInfoWindow();
		}
	 }




}